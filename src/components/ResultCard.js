import React from 'react';
import { MDBCard, MDBCardBody, MDBCardTitle, MDBCol, MDBListGroup, MDBListGroupItem, MDBBadge } from 'mdbreact';

const ResultCard = ({ data }) => {
  console.log(data)
  if (data.length === 0) {
    return (
      <MDBCol>
      <MDBCard style={{ height: "24rem" }}>
        <MDBCardBody>
          <MDBCardTitle><p className="h4 text-center py-4">Results</p></MDBCardTitle>
          <MDBListGroup>
          <MDBListGroupItem className="d-flex justify-content-between align-items-center">Country<MDBBadge color="primary"
              pill></MDBBadge>
            </MDBListGroupItem>
            <MDBListGroupItem className="d-flex justify-content-between align-items-center">Population<MDBBadge color="primary"
              pill></MDBBadge>
            </MDBListGroupItem>
            <MDBListGroupItem className="d-flex justify-content-between align-items-center">Area<MDBBadge
              color="primary" pill></MDBBadge>
            </MDBListGroupItem>
            <MDBListGroupItem className="d-flex justify-content-between align-items-center">Region<MDBBadge color="primary"
              pill></MDBBadge>
            </MDBListGroupItem>
            <MDBListGroupItem className="d-flex justify-content-between align-items-center">Capital<MDBBadge color="primary"
              pill></MDBBadge>
            </MDBListGroupItem>
          </MDBListGroup>
        </MDBCardBody>
      </MDBCard>
    </MDBCol>
    )
  } else {
  

  return (
    <MDBCol>
      <MDBCard style={{ height: "24rem" }}>
        <MDBCardBody>
          <MDBCardTitle><p className="h4 text-center py-4">Results</p></MDBCardTitle>
          <MDBListGroup>
          <MDBListGroupItem className="d-flex justify-content-between align-items-center">Country<MDBBadge color="primary"
              pill>{data[0].name}</MDBBadge>
            </MDBListGroupItem>
            <MDBListGroupItem className="d-flex justify-content-between align-items-center">Population<MDBBadge color="primary"
              pill>{data[0].population}</MDBBadge>
            </MDBListGroupItem>
            <MDBListGroupItem className="d-flex justify-content-between align-items-center">Area<MDBBadge
              color="primary" pill>{data[0].area}</MDBBadge>
            </MDBListGroupItem>
            <MDBListGroupItem className="d-flex justify-content-between align-items-center">Region<MDBBadge color="primary"
              pill>{data[0].region}</MDBBadge>
            </MDBListGroupItem>
            <MDBListGroupItem className="d-flex justify-content-between align-items-center">Capital<MDBBadge color="primary"
              pill>{data[0].capital}</MDBBadge>
            </MDBListGroupItem>
          </MDBListGroup>
        </MDBCardBody>
      </MDBCard>
    </MDBCol>
  )
}
}


export default ResultCard;
