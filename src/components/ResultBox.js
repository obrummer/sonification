import React from 'react';
import Jumbotron from './Jumbotron'
import InputCard from './InputCard'
import { MDBContainer, MDBRow, MDBCol } from "mdbreact";

function ResultBox() {
  return (
    <div>
      <Jumbotron />
      <MDBContainer>
      <MDBRow>
        <MDBCol md="12"><InputCard /></MDBCol>
      </MDBRow>
    </MDBContainer>
    </div>
  );
}

export default ResultBox;
