import React from "react";
import './Jumbotron.css';
import { MDBJumbotron, MDBContainer,MDBCol } from "mdbreact";

const Jumbotron = () => {
  return (
    <MDBJumbotron fluid style={{ backgroundImage: `url(https://mdbootstrap.com/img/Photos/Others/gradient1.jpg)` }}>
      <MDBContainer >
      <MDBCol>
        <h2 className="display-4">Listen to CountrySound</h2>
        <p className="lead">Find out what each country sounds based on data</p>
      </MDBCol>
      </MDBContainer>
    </MDBJumbotron>
  )
}

export default Jumbotron;
