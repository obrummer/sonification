import React from 'react'
import { shallow } from 'enzyme'
import App from '../../App'

let wrapped

beforeEach(() => {
    wrapped = shallow(<App />)
})

it('shows a navigation bar', () => {
    expect(wrapped.name()).toEqual('App')
})

// it('shows a result box', () => {
//     expect(wrapped.find(ResultBox).length).toEqual(1)
// })