import React from "react";
import ResultCard from './ResultCard';
import {
  RAudioContext,
  RBiquadFilter,
  RGain,
  ROscillator,
  RPipeline
} from 'r-audio';
import { MDBContainer, MDBBtn, MDBCard, MDBCardBody, MDBIcon, MDBRow, MDBCol } from 'mdbreact';
import axios from 'axios';

class InputCard extends React.Component {

  constructor(props) {
    super(props);

    this.nodeCache = [];
    this.handleSubmit = this.handleSubmit.bind(this);

    this.state = {
      data: [],
      nodes: this.nodeCache,
      toggle: true
    }
    this.change = () => {
      const changed = this.nodeCache.slice();
      changed.splice(1, 1, <ROscillator start={0} key={0} frequency={this.state.data[0].area / 1000} type="triangle" />);
      // changed.splice(2, 1, <ROscillator start={0} key={1} frequency={this.state.lat} type="triangle" />);
      // changed.splice(3, 1, <ROscillator start={0} key={2} frequency={this.state.lon} type="triangle" />);
      // changed.splice(4, 1, <ROscillator start={0} key={3} frequency={(this.state.wind) * 10} type="triangle" />);            
      changed.splice(2, 1, <RBiquadFilter start={0} key={1} type="lowpass" frequency={500} Q={440} />);
      changed.splice(3, 1, <RGain start={0} key={2} gain={0.2} />);
      this.setState({ nodes: changed });
    }
  }


  handleSubmit(e) {
    e.preventDefault();

    const formData = {};
    for (const field in this.refs) {
      formData[field] = this.refs[field].value;
    }
    console.log('-->', formData);


    axios.get('https://restcountries.eu/rest/v2/name/' + formData.country)
      .then(res => {
        const data = res.data;
        this.setState({ data });
        console.log(data)
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
  }

  render = () => {
    console.log(this.state.nodes)
    return (
      <MDBContainer>
        <MDBRow>
          <MDBCol md='6'>
            <MDBCard style={{ height: "24rem" }}>
              <MDBCardBody>
                <form onSubmit={this.handleSubmit}>
                  <p className="h4 text-center py-4">Input country name</p>
                  <label
                    htmlFor="defaultFormCardNameEx"
                    className="grey-text font-weight-light"
                  >
                    Search country
                </label>
                  <input
                    type="text"
                    ref="country"
                    id="defaultFormCardNameEx"
                    className="form-control"
                  />
                  <br />
                  <div className="text-center py-4 mt-3">
                    <MDBBtn className="btn btn-outline-purple" type='submit'>
                      Search
                    <MDBIcon far icon="paper-plane" className="ml-2" />
                    </MDBBtn>
                    <MDBBtn className="btn btn-outline-purple" onClick={this.change} >
                      Sonify
                    <MDBIcon icon="play" className="ml-2" />
                    </MDBBtn>
                  </div>
                </form>
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
          <MDBCol md='6'>
            <ResultCard data={this.state.data} />
          </MDBCol>
        </MDBRow>
        
        <RAudioContext>
          <RPipeline>{this.state.nodes}</RPipeline>
        </RAudioContext>

      </MDBContainer>

    );
  };
}

export default InputCard;