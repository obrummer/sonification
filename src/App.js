import React from 'react';
import Navigation from './Navigation'
import ResultBox from './components/ResultBox'
import './App.css';

function App() {
  return (
    <div>
      <Navigation />
      <ResultBox />
    </div>
  );
}

export default App;
